const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema = mongoose.Schema;
const goodsTypeSchema = new Schema({
   value: {
       type: Number
   },
    label: {
       type: String,
    },
    children: {
       type: Array
    }
})

module.exports = new mongoose.model('GoodsType', goodsTypeSchema)