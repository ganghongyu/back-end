const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema = mongoose.Schema;
const couponSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: new Date
    },
    value: {
        type: String,
        default: Date.now
    },
    label: {
        type: String,
        required: true,
    },
    coupon: {
        type: Number,
        required: true
    }
})
module.exports = new mongoose.model('Coupon', couponSchema)