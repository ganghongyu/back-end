const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema = mongoose.Schema;
const userSchema = new Schema({
    nickname: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['1','2'],
        default: '1'
    },
    address: {
        // 地址
        type: Array,
        default: []
    },
    income: {
        // 收入
        type: String,
        default: ''
    },
    education: {
        type: String,
        default: ''

    },
    integral: {
        type: Number,
        default: 0

    },
    avatar: {
        type: String,
        default: 'http://localhost:3000/public/img/avatar/head_pic.jpg'
    },
    favorites: {
        type: Array
    },
    footFrint: {
        type: Array
    },
    luckNumber: {
        type: Number,
        default: 0
    }
})
module.exports = new mongoose.model('User', userSchema);