const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema = mongoose.Schema;
const commentSchema = new Schema({
    userId: {
        type: String,
    },
    date: {
        type: Date,
        default: new Date
    },
    goodsId: {
        type: String
    },
    message: {
        type: String,
    },
    star: {
        type: Number
    }
})
module.exports = new mongoose.model('Comment', commentSchema)