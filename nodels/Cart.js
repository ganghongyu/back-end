const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema = mongoose.Schema;
const cartSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    carts: {
        type: Array,
    }
})
module.exports = new mongoose.model('Cart', cartSchema)