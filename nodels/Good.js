const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema =  mongoose.Schema;
const goodSchema = new Schema({
    name: {
        // 商品信息
        type: String,
    },
    price: {
        // 商品价格
        type: String,
    },
    describe: {
        // 商品描述
        type: String,
    },
    stock: {
        // 商品库存
        type: Number,
    },
    category: {
        // 商品种类
        type: Array,
    },
    paramsFilename: {
        // 商品参数上传文件名
        type: Array
    },
    descFilename: {
        // 商品描述图片文件名
        type: Array
    },
    goodsFilename: {
        // 商品图片文件名
        type: Array
    },
    params: {
        // 参数名字+商品文件路径 如： 红色等
        type: Array,

    },
    desImg: {
        // 商品描述文件路径
        type: Array,
    },
    goodsImg: {
        // 商品图片路径存贮
        type: Array
    },
    isSell: {
        type: Boolean,
        default: true
    },
    date: {
        type: Number,
        default: new Date().getTime()
    },
    // 收藏人数
    favorites: {
        type: Number,
        default: 0
    },
    // 评分
    star: {
        type: Number,
        default: 0
    },
    salesd: {
        type: Number,
        default: 0
    }
})
module.exports = new mongoose.model("Good", goodSchema)