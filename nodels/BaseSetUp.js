const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema =  mongoose.Schema;
const baseSetSchema = new Schema({
    firstSwiper: {
        type: Array,
    },
    secondSwiper: {
        type: Array,
    },
    recommends: {
        type: Array
    },
    brand: {
        type: String,
        default: '母婴销售系统'
    },
    logoUrl: {
        type: String,
        default: 'http://localhost:3000/public/img/logo/logo.png'
    }
})
module.exports = new mongoose.model('baseSet', baseSetSchema)