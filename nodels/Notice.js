const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema =  mongoose.Schema;
const noticeSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true,
    },
    content: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        default: new Date
    }
})
module.exports = new mongoose.model('Notice', noticeSchema)