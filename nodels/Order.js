const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema =  mongoose.Schema;
const orderSchema = new Schema({
    goodsMessage: {
        type: Array,
        required: true
    },
    address: {
      type: Object,
      required: true
    },
    userId: {
        type: String
    },
    // 是否发出
    lssue: {
        type: Boolean,
        default: false
    },
    // 快递单号
    delivery: {
        type: Number,
    },
    time: {
        type: Date,
        default: new Date
    }
})
module.exports = new mongoose.model("order", orderSchema)