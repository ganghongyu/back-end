const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/motherandbodymall');
const Schema = mongoose.Schema;
const businessSchema = new Schema({
    isSuper: {
        type: Boolean,
        default: false
    },
    nickname: {
        type: String,
    },
    account: {
        type: Number,
    },
    password: {
        type: String,
    },
    tel: {
        type: Number,

    },
    jurisdiction: {
        type: Array
    }

})
module.exports = new mongoose.model('BusinessAccount', businessSchema)