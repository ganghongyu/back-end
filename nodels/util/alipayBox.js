const AlipaySdk = require('alipay-sdk').default
const AlipayFormData = require('alipay-sdk/lib/form').default

const alipaySdk = new AlipaySdk({
    appId: '2016101900722220',
    gateway: 'https://openapi.alipaydev.com/gateway.do',
    signType: 'RSA2',
    charset: 'utf-8',
    privateKey: 'MIIEowIBAAKCAQEAhQl/hAoTzLF9YAxlYRltavxP3cNSiIWWrCxHIg7VDr94frv9syclPpbm6ZhGPdZsWuDPUxku1N1eUB7ioNbqfqXnEEjA1Uz3n91G0MAQFosbwjubXDox/d1pJpX8n9hgYOsxuZFpzmGiNfmPqFplxA02sdtyUD4zlVR4+SLwlkohErcuAiYKm4IlMP0Rwd53n0yhpkQUkOK8aaqeLX9751bzvD0Y4vytfdsyS5J5zsgNBfk8c/+B2CxZJulRJ7F/CbDh58DPKuhI56CtNJMffiUGllYiwouUkgrhnQaT+Bo5sy40Yv8mWz3w+BcxDlZnQFkKyAfO+HwaopZhwszWDQIDAQABAoIBAFYrZcXPhS2TVSLuRIgVITMnxE9eoUm+RW61q0lbFBNeX5yvBU+SLmSQ93YcZHrPNRXUAVXQvO2xiNIafy4wpmhr6vM4PHasSMPqXmD91i3T4qH3W9Tr+M93JxGQGrYqP5rnHCkXD9JWmOue/rbL/phy+pdPDYb+EwlJUqYERJMC9je6bN6hBXXonRAp6pxVLXchjh80dnnzKmlL3WihoFZiurieD6paYSjYgvLKY6Ok6vzI30q5j4aTMT3Kzd502C9K6yRrOxsW+2zSnuGRKYohVDhMBcUUXDQyBbhH8MqUpNwIgCuP0fqMbCq5/sPCSymtU1OT0uuL+M0KChMO9eECgYEAzrKdcS5PBEw15AF5AnSkeBgaAFNOhEBdL/2qt1c4DY3kQnPnR7zf7VOv+s1pMNpL7T7XRGp3F319A7Zpxuhgwf5Ek6dk1TtbrUnTXD43pxgLJFMpgqhiOYbeAsER/dBrIc3paExFkW6O2vkVQnqf+tN9bY6OJWWadtK0uW8UKJkCgYEApMUD+A4qBP7XScAp9iPKWg9j9Ev6ZXOOelhKFoNl4vjoOC044rz1p/fKLtfGw4e3dQAbLw7v9McQxJYo+fjDX5jZlikEmXKURRaRXb8VxzN6ykSrIePDZEqfpH9lsvwmuRZ9W8ocMY2qsn5dQkeMuKRpDDQI2poFwYy3wUzC/ZUCgYEAn8Ovriv9J3VAslvxDNTby3fVoSrDJbYaUV3g9pWT3eYu6K5ttiVncCmffFwJUplo1pgcC+W+54k3eAcm+3fiUhAXMYnRhn+6/NZzaHkuyxjqg8lD7Jn6UjzMnZReM7FFHXvrTZKmsyKhiN++bwQZY+bUrDnj5UQDgJSc3DfffQECgYAJG9cfVHEgO5JdQmFPeSULm4Q0EisyhpPe0Ziu6+6rAIWd7WoSps2RzpH/yu/zCUKsFihqjpHXX1Xe+Eoln4n1c/a7ZWMatInb0+aPXyvuCjuEiQ1I31WC6tkud9iAb44Yd1TQyTiLyMAiA/hV3Ky73GLuSMYgh5k/3O6fogW5jQKBgF/6+Zqg88O9Dt0szjE22I3azeQ8xaUxJ0B4VvkZWQi8aGHYHls1lS/1rK1UPsDgJPM0rCE83Fq/deYWNXyn0uphZxqUQ/PhvTbwGHOY466CiAJVwhkrE5hdC/P1vZYQX5kIDM3mVF6JH4lqZypeRQ79VGqx/Wh3EUhiqXB4zw5S',
    alipayPublicKey: 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnkVGaGikyH9rTJ3mHfcGrXGRNgy7MerO1WluC84p6NjoHOZVA/tsQEfj60GBQHkkBzorFmbcJRMVLENXP0RxM6NnbKob5DQNVsLIFu53UKL/cU9keBJPUp7OGrt/Urp0uDsrPkYfxz6YGxlJ5vqNlqygUYgDMMTc0Rt+MWBG+HNnS+d3RP7uSn3NFa2o6Yp7p94gO0E8zsH1f1dfDAZ4i0tmNDu4xlS3G3ihSyjFW5PXVsgW/nWGL241TJgLUAHwL4//NKMO8FZxLGWpW5oMVw1r5Ftdne41gywjs7sylgZO5UTtlKuafAYYrDsuR/WJNlK0d8CNepjJmihm4l0dcwIDAQAB'
})

const alipayBox = {
    pay: async function (sumPrice) {
        const formData = new AlipayFormData()
        // 调用 setMethod 并传入 get，会返回可以跳转到支付页面的 url
        formData.setMethod('get')
        // 配置回调接口
        formData.addField('notifyUrl', 'http://localhost:3000/callpay')
        formData.addField('returnUrl', 'http://localhost:3000/callpay')
        // 设置参数
        formData.addField('bizContent', {
            outTradeNo: new Date().getTime(),
            productCode: 'FAST_INSTANT_TRADE_PAY',
            totalAmount: sumPrice,
            subject: '商品',
            body: '商品详情',
        });
        // 请求接口
        const result = await alipaySdk.exec(
            'alipay.trade.page.pay',
            {},
            { formData: formData },
        );
        return result
    }

}
module.exports = alipayBox