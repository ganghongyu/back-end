const express = require('express');
const router = require('./router');
const bodyParser = require('body-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const history = require('connect-history-api-fallback')
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server)
let customer = {}
let business = {}
io.on('connection', (socket)=> {
    // 用户登录
    socket.on('lognIn', (data)=> {
        customer[data] = socket

    })
    // 商家登录
    socket.on('businessLogn',(data)=> {
            business[data] = socket
    })
    // 发送消息
    socket.on('msg',(data)=> {
        // console.log(business)
        for (let i in business) {
            business[i].emit('sendToBusiness', data)
        }
    })
    socket.on('replay', (data)=> {
        customer[data.toId].emit('sendToCustomer', data)
    })

})
app.use(session({
    secret: 'motherandbobymall',
    resave: false,
    saveUninitialized: true,
    // // cookie: { secure: true }
    store: new MongoStore({
       url: 'mongodb://localhost/motherandbobymall-session'
    })

}))
app.use('/public/', express.static('./public/'))
app.use(express.static('./dist/'))
app.use('/public/', express.static('/public/'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extends: false}));
app.use(history({
    // verbose: true,
    index: '/'
}))
app.use(router)

server.listen('3000', function () {
    console.log('running....')
})